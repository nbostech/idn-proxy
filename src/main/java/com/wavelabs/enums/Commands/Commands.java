package com.wavelabs.enums.Commands;

public enum Commands {

	signup("signup"), login("login"), createTenant("create-tenant"), createClient("create-client");
	private String str = null;

	private Commands(String value) {
		str = value;
	}

	public String getString() {
		return str;
	}
}
