package com.wavelabs;

public class Tenant {

	private int id;
	private String name;
	private String tenantId;
	private Client[] client;
	
	public Client[] getClient() {
		return client;
	}
	public void setClient(Client[] client) {
		this.client = client;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
