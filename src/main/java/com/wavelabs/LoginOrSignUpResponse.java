package com.wavelabs;

public class LoginOrSignUpResponse {

	Token token;
    String host="http://api.qa1.nbos.in";
	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
}
