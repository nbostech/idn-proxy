package com.wavelabs.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.wavelabs.LoginOrSignUpResponse;
import com.wavelabs.Tenant;
import com.wavelabs.service.CommandService;

/**
 * 
 * @author gopikrishnag
 *
 */
@RestController
public class CommandController {

	@Autowired
	private CommandService service;

	@RequestMapping("/{command}")
	public String parseCommand(@RequestParam("args") String[] args, @PathVariable("command") String command)
			throws ParseException, IOException {
		if ("signup".equalsIgnoreCase(command)) {
			return service.signUpUser(args);
		} else if ("login".equals(command)) {
			return service.loginUser(args);
		} else if ("create-tenant".equals(command)) {
			return "\n" + service.createTenant(args);
		} else if ("create-client".equals(command)) {
			return service.createClient(args);
		} else if ("show-clients".equals(command)) {

		} else if ("logout".equals(command)) {
			return service.logout();
		} else if ("show-tenants".equals(command)) {

		}
		return null;
	}

	@RequestMapping("/tenants")
	public Tenant[] getAllTenants(@RequestParam("args") String[] args)
			throws ParseException, JsonIOException, JsonSyntaxException, FileNotFoundException {
		return service.getTenants(args);
	}

	@RequestMapping(method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, value = "tenant/show-all", produces= MediaType.APPLICATION_JSON_VALUE)
	public String listAllTenants(@RequestBody Tenant[] tenants) {
		Map<String, String> map = new HashMap<String, String>();
		for (Tenant tenant : tenants) {
			map.put(tenant.getTenantId(), tenant.getName());
			System.out.println(tenant.getId());
		}
		Gson gson = new Gson();
		String json = gson.toJson(map);
		
		return json;
	}
}
