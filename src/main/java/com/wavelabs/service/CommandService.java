package com.wavelabs.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.wavelabs.Client;
import com.wavelabs.LoginOrSignUpResponse;
import com.wavelabs.Tenant;
import com.wavelabs.filesystem.FileSystemServicecs;
import com.wavelabs.utility.OptionsUtility;

/**
 * This class has Methods to run nbos commands like signup, login, create-tenant
 * etc.,
 * 
 * @author gopikrishnag
 *
 */
@Component
public class CommandService {

	private static final String SIGNUP = "http://api.qa1.nbos.in/api/identity/v0/users/signup";
	private static final String LOGIN = "http://api.qa1.nbos.in/api/identity/v0/auth/login";
	private static final String CREATE_TENANT = "http://api.qa1.nbos.in/api/tenant/v0/tenants";
	private static final String LIST_OF_TENANTS = "http://api.qa1.nbos.in/api/tenant/v0/tenants";

	public String signUpUser(String[] args) throws ParseException {
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(OptionsUtility.getOptionsForSignUp(), args);
		String email = cmd.getOptionValue("email");
		String input = "{\"clientId\":\"appConsole-app-client\",\"username\":\"" + email + "\",\"firstName\":\""
				+ cmd.getOptionValue("fn") + "\",\"lastName\":\"" + cmd.getOptionValue("ln") + "\",\"email\":\"" + email
				+ "\",\"password\":\"" + cmd.getOptionValue("password") + "\"}";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer 96b23054-79e8-4874-b0b2-0d99aaa3d179");
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		HttpEntity<String> entity = new HttpEntity<>(input, headers);
		ResponseEntity<LoginOrSignUpResponse> response = restTemplate.exchange(SIGNUP, HttpMethod.POST, entity,
				LoginOrSignUpResponse.class);
		if ((response.getBody().getToken().getAccess_token()) != null) {

			return "Account created successfully, Please login using nbos login email password";
		} else {
			return "Problem in creation! Try again later or try with different credentials";
		}
	}

	public String createClient(String[] args) throws ParseException, JsonIOException, JsonSyntaxException, IOException {

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(OptionsUtility.getOptionsForCreateClient(), args);
		String url = "http://api.qa1.nbos.in/api/oauth/v0/tenants/" + cmd.getOptionValue("t") + "/clients";
		String input = "{\"authorities\":\"[]\",\"authorizedGrantTypes\":[\"client_credentials\",\"refresh_token\"],\"resourceIds\":\"[]\",\"scopes\":\"[]\",\"redirectUris\":\"[]\",\"tenantId\":\""
				+ cmd.getOptionValue("t") + "\",\"clientName\":\"" + cmd.getOptionValue("cn") + "\",\"clientSecret\":\""
				+ cmd.getOptionValue("cs") + "\"}";
		HttpHeaders headers = new HttpHeaders();
		if (isUserLoggedIn()) {
			headers.set("Authorization", "Bearer " + FileSystemServicecs.getAccessToekn());
			headers.setContentType(MediaType.APPLICATION_JSON);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			HttpEntity<String> entity = new HttpEntity<String>(input, headers);
			ResponseEntity<Client> response = restTemplate.exchange(url, HttpMethod.POST, entity, Client.class);
			System.out.println(response.getBody().getClientName());
			return "\n Client ID: \n" + response.getBody().getClientId();
		} else {
			return "\n Please login \n";
		}
	}

	public String loginUser(String[] args) throws ParseException, IOException {
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(OptionsUtility.getOptionsForLogIn(), args);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer 96b23054-79e8-4874-b0b2-0d99aaa3d179");
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		String in = "{\"username\":\"" + cmd.getOptionValue("u") + "\",\"password\":\"" + cmd.getOptionValue("p")
				+ "\"}";
		HttpEntity<String> entity = new HttpEntity<String>(in, headers);
		ResponseEntity<LoginOrSignUpResponse> response = restTemplate.exchange(LOGIN, HttpMethod.POST, entity,
				LoginOrSignUpResponse.class);
		System.out.println((response.getBody().getToken().getAccess_token()));
		String accessToken = response.getBody().getToken().getAccess_token();
		if (accessToken != null) {
			FileSystemServicecs.saveAccessToken(response.getBody());
			//getTenants();
		}
		System.out.println(response.getBody().getToken().getAccess_token());
		return response.getBody().getToken().getAccess_token();
	}

	public String createTenant(String[] args) throws ParseException, JsonIOException, JsonSyntaxException, IOException {

		//String accessToken = FileSystemServicecs.getAccessToekn();
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(OptionsUtility.getOptionsForTenantCreation(), args);
		String accessToken = cmd.getOptionValue("a");
		if (accessToken == null) {
			return "\n Please login \n";

		} else {
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Bearer " + accessToken);
			headers.setContentType(MediaType.APPLICATION_JSON);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			String tenant = "{\"id\": \"0\", \"tenantId\": \"\", \"isPublic\": \"true\", \"apiClientSecret\": \"API_CLIENT_SECRET\", \"name\": \""
					+ cmd.getOptionValue("t") + "\"}";

			HttpEntity<String> entity = new HttpEntity<String>(tenant, headers);
			ResponseEntity<Tenant> response = restTemplate.exchange(CREATE_TENANT, HttpMethod.POST, entity,
					Tenant.class);
			System.out.println(response.getBody().getName() + " " + response.getBody().getTenantId());
			//getTenants();
			return "\n TenantId: " + response.getBody().getTenantId()+" \n ";
		}
	}

	public static String logout() throws JsonIOException, JsonSyntaxException, IOException {

		String accessToken = FileSystemServicecs.getAccessToekn();
		if (!isUserLoggedIn()) {

			return "\n You are not logged in yet \n ";

		} else {
			File folder = new File(FileSystemServicecs.getUserHomeDirectory() + "/.idn/");
			File[] listOfFiles = folder.listFiles();
			for (File tempFile : listOfFiles) {
				Path path = FileSystems.getDefault().getPath(FileSystemServicecs.getUserHomeDirectory() + "/.idn",
						tempFile.getName());
				Files.delete(path);
			}
			FileUtils.forceDelete(new File(FileSystemServicecs.getUserHomeDirectory() + "/.idn"));
			if (!isUserLoggedIn()) {
				return "\n Successfully logged out \n";
			} else {
				return "\n Error occured, Try after some time \n";
			}
		}
	}

	public static boolean isUserLoggedIn() throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		String accessToken = FileSystemServicecs.getAccessToekn();
		if (accessToken == null) {
			return false;
		}
		return true;
	}



	public static String getTenants() throws JsonIOException, JsonSyntaxException, IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + FileSystemServicecs.getAccessToekn());
		RestTemplate template = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> response = template.exchange(LIST_OF_TENANTS, HttpMethod.GET, entity, String.class);
		Gson gson = new Gson();
		Tenant[] tenants = gson.fromJson(response.getBody(), Tenant[].class);
		getClients(tenants);
		return null;
	}
	public static Tenant[] getTenants(String[] args) throws ParseException, JsonIOException, JsonSyntaxException, FileNotFoundException {
		
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(OptionsUtility.getOptionsForTenantCreation(), args);
		HttpHeaders headers = new HttpHeaders();
		System.out.println(cmd.getOptionValue("a"));
		headers.set("Authorization", "Bearer " + cmd.getOptionValue("a"));
		RestTemplate template = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ResponseEntity<String> response = template.exchange(LIST_OF_TENANTS, HttpMethod.GET, entity, String.class);
		Gson gson = new Gson();
		Tenant[] tenants = gson.fromJson(response.getBody(), Tenant[].class);
		Tenant[] tenantsWithClient = new Tenant[tenants.length];
		for(int i=0;i<tenants.length;i++) {
			tenantsWithClient[i]=getClientsWithTenant(tenants[i]);
		}
		return tenantsWithClient;
	}
	
	
	
	public static Tenant getClientsWithTenant(Tenant tenant)
			throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + FileSystemServicecs.getAccessToekn());
		RestTemplate template = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			String url = "http://api.qa1.nbos.in/api/oauth/v0/tenants/" + tenant.getTenantId() + "/clients";
			ResponseEntity<String> response = template.exchange(url, HttpMethod.GET, entity, String.class);
			Gson gson = new Gson();
			Client[] clients = gson.fromJson(response.getBody(), Client[].class);
			tenant.setClient(clients);
		return tenant;
	}
	
	public static Client[] getClients(Tenant[] tenants)
			throws JsonIOException, JsonSyntaxException, FileNotFoundException {

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + FileSystemServicecs.getAccessToekn());
		RestTemplate template = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		for (Tenant tenant : tenants) {
			String url = "http://api.qa1.nbos.in/api/oauth/v0/tenants/" + tenant.getTenantId() + "/clients";
			ResponseEntity<String> response = template.exchange(url, HttpMethod.GET, entity, String.class);
			Gson gson = new Gson();
			Client[] clients = gson.fromJson(response.getBody(), Client[].class);
			tenant.setClient(clients);
			FileSystemServicecs.createTenantFile(tenant);
		}

		return null;
	}
}
