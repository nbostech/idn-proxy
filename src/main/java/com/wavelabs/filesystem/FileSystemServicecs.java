package com.wavelabs.filesystem;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.wavelabs.LoginOrSignUpResponse;
import com.wavelabs.Tenant;
import com.wavelabs.Token;
/**
 * This class has methods for fileOpeartions to store and retrieve the file data
 * @author gopikrishnag
 *
 */
public class FileSystemServicecs {

	private FileSystemServicecs() {

	}

	public static String saveAccessToken(LoginOrSignUpResponse lsr) throws IOException {
		String userHome = getUserHomeDirectory();
		String directoryName = ".idn";
		File file = new File(userHome + "/" + directoryName);
		if (!file.exists()) {
			file.mkdir();
		}
		/* File configFile = new File(directoryName+"/config.json"); */
		Gson gson = new Gson();
		FileWriter configFile = new FileWriter(userHome + "/" + directoryName + "/config.json");
		configFile.write(gson.toJson(lsr));
		configFile.flush();
		configFile.close();
		return null;
	}

	public static String findOS() {
		return System.getProperty("os.name");
	}

	public static String getUserHomeDirectory() {
		String osName = findOS();
		if("Windows 7".equals(osName)) {
			return System.getenv("USERPROFILE");
		}
		else {
			return System.getenv("HOME");
		}
	}

	public static String getAccessToekn() throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		String userHome = FileSystemServicecs.getUserHomeDirectory();
		JsonParser parser = new JsonParser();
		try {
		FileReader reader = new FileReader(userHome + "/.idn/config.json");	
		Object obj = parser.parse(reader);
		JsonObject jsonObject = (JsonObject) obj;
		JsonObject jsonElement = jsonObject.getAsJsonObject("token");
		Gson gson = new Gson();
		Token token = gson.fromJson(jsonElement, Token.class);
		reader.close();
		return token.getAccess_token();
		}
		catch (Exception e) {
			return null;
		}
	}
	public static boolean createTenantFile(Tenant tenant) {
		
		String userHome = getUserHomeDirectory();
		try {
			FileWriter fileWriter = new FileWriter(userHome+"/.idn/"+tenant.getName()+".json");
			Gson gson = new Gson();
			String tenantJson = gson.toJson(tenant);
			fileWriter.write(tenantJson);
			fileWriter.flush();
			fileWriter.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}
}
